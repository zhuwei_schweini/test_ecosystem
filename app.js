var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var favicon = require('serve-favicon');
var mongoose = require('mongoose');
require('dotenv').config();

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();
const uri = 'mongodb://192.168.16.166/testdb';
//const uri = 'mongodb://172.26.9.73:27017,172.26.3.26:27017,172.26.40.122:27017/testdb';

// env to config
var config = 
{
  node_env : process.env.NODE_ENV || "development",
  port :  process.env.PORT || 3001,
  log_path : process.env.LOG_PATH || "/logs",
  db : process.env.DB,
  jwt_sceret : process.env.JWT_SECRET,
  session_time : parseInt(process.env.ONLINE_SESSION_TIME) || 2,
  path : path.join(__dirname),
  IPLOC_KEY : process.env.IPLOC_KEY,
  OPENWEATHER_KEY : process.env.OPENWEATHER_KEY,
  WEATHERCOM_KEY : process.env.WEATHERCOM_KEY,
  isProduction : (cfg) => cfg.node_env!=="development",
}

console.log ("CONFIG : NODE_ENV [%s]", process.env.NODE_ENV);
console.log ("CONFIG : DB [%s]", process.env.DB);
console.log ("CONFIG : PORT [%s]", config.port);
console.log ("CONFIG : JWT_SECRET [%s]", process.env.JWT_SECRET);
console.log ("CONFIG : IPLOC_KEY [%s]", config.IPLOC_KEY);
console.log ("CONFIG : OPENWEATHER_KEY [%s]", config.OPENWEATHER_KEY);
console.log ("CONFIG : WEATHERCOM_KEY [%s]", config.WEATHERCOM_KEY);
console.log ("CONFIG : ONLINE_SESSION_TIME [%s]", process.env.ONLINE_SESSION_TIME || "2");
console.log ("CONFIG : LOG PATH [%s]", process.env.LOG_PATH || "logs");

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(3001, function() {
	console.log('listening in 3001');
});

var db = mongoose.connection;

db.on("open", function() {
	console.log('db is opened');
});

db.on("error", function(err) {
	console.log('db Error: ' + err);
});

mongoose.connect(process.env.DB, { useNewUrlParser: true});
mongoose.set('useCreateIndex', true);

module.exports = app;
