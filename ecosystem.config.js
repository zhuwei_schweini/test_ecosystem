module.exports = {
  apps : [{
    name: 'test_ecosystem',
    script: 'app.js',
    
    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    args: '',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development',
      DB: "mongodb://192.168.16.166/testdb",
      JWT_SECRET: "15567b09-88d2-45ba-9ddc-00314dde41a9"
    },
    env_testing: {
      NODE_ENV: 'testing',
      DB: "mongodb://172.26.9.73:27017,172.26.3.26:27017,172.26.40.122:27017/wlink",
      IPLOC_KEY: "c69c3adac5eee95f8f9078180bde0ff0",
      OPENWEATHER_KEY: "1aef6a78b0c9ffc201fadf9954babc25",
      WEATHERCOM_KEY: "",
      ONLINE_SESSION_TIME: 576,
      LOG_PATH: "logs"
    },
    env_production: {
      NODE_ENV: 'production',
      DB: "mongodb://172.26.9.73:27017,172.26.3.26:27017,172.26.40.122:27017/wlink",
      IPLOC_KEY: "c69c3adac5eee95f8f9078180bde0ff0",
      OPENWEATHER_KEY: "1aef6a78b0c9ffc201fadf9954babc25",
      WEATHERCOM_KEY: "",
      ONLINE_SESSION_TIME: 576,
      LOG_PATH: "logs"
    }
  }],
  
  deploy : {
    production : {
      user : 'ubuntu',
      host : ['api1.wavlink.xyz', 'api2.wavlink.xyz'],
      ref  : 'origin/master',
      repo : 'https://zhuwei_schweini@bitbucket.org/zhuwei_schweini/test_ecosystem.git',
      path : '/home/ubuntu/zhuwei/test_ecosystem',
      ssh_options: ['ForwardAgent=yes'],
      'post-deploy' : 'yarn && pm2 reload ecosystem.config.js --env production'
    },
    testing : {
      user : 'ubuntu',
      host : '18.136.3.87',
      ref  : 'origin/master',
      repo : 'https://zhuwei_schweini@bitbucket.org/zhuwei_schweini/test_ecosystem.git',
      path : '/home/ubuntu/service/zhuwei/test_ecosystem',
      ssh_options: ['ForwardAgent=yes'],
      'post-deploy' : 'yarn && pm2 reload ecosystem.config.js --env testing'
    },
    staging: {
      user : 'kobe',
      host : '192.168.16.188',
      ref  : 'origin/master',
      repo : 'https://zhuwei_schweini@bitbucket.org/zhuwei_schweini/test_ecosystem.git',
      path : '/home/kobe/zhuwei_test/test_ecosystem',
      ssh_options: ['ForwardAgent=yes'],
      'post-deploy' : 'yarn && pm2 reload ecosystem.config.js --env development'
    },
    dev: {}
  } 
};
