const mongoose = require('mongoose');

// const uri = 'mongodb://172.26.9.73:27017,172.26.3.26:27017,172.26.40.122:27017/testdb';

// mongoose.connect(uri, function(err) {
//     if (err) {
//         console.log('connect failed');
//         console.log(err);
//         return;
//     }
//     console.log('connect success');
// });

const StudentSchema = new mongoose.Schema({
    name: String,
    chinese: Number,
    english: Number,
    math:    Number
}, { collection: 'students' });

StudentSchema.index({name: 1});
StudentSchema.index({chinese: 1});
StudentSchema.index({english: 1});
StudentSchema.index({math: 1});

module.exports = mongoose.model('student', StudentSchema);

// Student.find({}, function(err, docs) {
//     if (err) {
//         console.log(err);
//         returnl
//     }
//     console.log("return: " + docs);
// });
